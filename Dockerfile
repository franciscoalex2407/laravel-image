# Imagem base
FROM php:8.2-fpm-alpine AS base

# Instalar dependências do sistema
RUN apk add --no-cache \
    git \
    zip \
    unzip \
    icu-dev \
    oniguruma-dev \
    autoconf \
    g++ \
    make \
    postgresql-dev \
    libzip-dev \
    freetype-dev \
    libjpeg-turbo-dev \
    libpng-dev \
    supervisor && \
    docker-php-ext-configure gd --with-freetype=/usr/include/ --with-jpeg=/usr/include/ && \
    docker-php-ext-install intl mbstring pdo_mysql pdo_pgsql mysqli zip pcntl -j$(nproc) gd

# Instalar a extensão Swoole
RUN pecl install swoole \
    && docker-php-ext-enable swoole

# Instalar a extensão Redis
RUN pecl install redis \
    && docker-php-ext-enable redis

# Instalar Composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer
